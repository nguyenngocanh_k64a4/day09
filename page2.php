<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kiểm tra</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        *:focus {
            outline: none;
        }

        /* Customize radio button */
        input[type="radio"]:focus {
            outline:none;
        }

        [type="radio"]:checked,
        [type="radio"]:not(:checked) {
            position: absolute;
            left: -9999px;
        }
        [type="radio"]:checked + label,
        [type="radio"]:not(:checked) + label
        {
            position: relative;
            padding-left: 28px;
            cursor: pointer;
            line-height: 20px;
            display: inline-block;
            color: #666;
        }
        [type="radio"]:checked + label:before,
        [type="radio"]:not(:checked) + label:before {
            content: '';
            position: absolute;
            left: 0;
            top: 0;
            width: 18px;
            height: 18px;
            border: 1px solid #ddd;
            border-radius: 100%;
            background: #fff;
        }
        [type="radio"]:checked + label:after,
        [type="radio"]:not(:checked) + label:after {
            content: '';
            width: 10px;
            height: 10px;
            background: #F87DA9;
            position: absolute;
            top: 4px;
            left: 4px;
            border-radius: 100%;
            -webkit-transition: all 0.2s ease;
            transition: all 0.2s ease;
        }
        [type="radio"]:not(:checked) + label:after {
            opacity: 0;
            -webkit-transform: scale(0);
            transform: scale(0);
        }
        [type="radio"]:checked + label:after {
            opacity: 1;
            -webkit-transform: scale(1);
            transform: scale(1);
        }

        /* Style */

        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
            font-size: 16px;
        }

        .wrapper {
            width: 45%;
            padding: 20px 60px 100px;
            margin-top: 4px;
            margin-bottom: 60px;
            position: relative;
            border: 1px solid #ccc;
        }

        .title {
            text-align: center;
            color: rgba(50, 96, 168, 0.94);
            margin-bottom: 40px;
        }

        .question__content {
            color: #2E8BC0;
        }

        .question {
            margin-top: 30px;
        }

        label {
            cursor: pointer;
            font-weight: 400;
        }

        .btn-submit {
            font-size: 16px;
            color: #eeeeee;
            background-color: #4480e1;
            padding: 12px 32px;
            margin-top: 15px;
            border-radius: 10px;
            position: absolute;
            bottom: 40px;
            right: 60px;
        }

    </style>
</head>
<body>
    <div class="main">
        <?php
            class Question
            {
                public $content;
                public $answerA;
                public $answerB;
                public $answerC;
                public $answerD;
                public $correctAnswer;
            }

            $question1 = new Question();
            $question1->content = 'Vĩ tuyến 17 chạy qua địa phận tỉnh nào của nước ta?';
            $question1->answerA = 'Quảng Bình.';
            $question1->answerB = 'Quảng Trị.';
            $question1->answerC = 'Thanh Hóa.';
            $question1->answerD = 'Nghệ An.';
            $question1->correctAnswer = 'B';

            $question2 = new Question();
            $question2->content = 'Nằm bên dãy Hoàng Liên Sơn, Mù Căng Chải là huyện vùng sâu vùng xa của tỉnh nào?';
            $question2->answerA = 'Hà Giang.';
            $question2->answerB = 'Cao Bằng.';
            $question2->answerC = 'Yên Bái.';
            $question2->answerD = 'Lào Cai.';
            $question2->correctAnswer = 'C';

            $question3 = new Question();
            $question3->content = '"Bao giờ hết cỏ nước Nam thì hết người Nam đánh Tây" – Đây là câu nói của ai?';
            $question3->answerA = 'Ngô Thì Nhậm.';
            $question3->answerB = 'Trần Cao Vân.';
            $question3->answerC = 'Nguyễn Trung Trực.';
            $question3->answerD = 'Ngô Đức Kế.';
            $question3->correctAnswer = 'C';

            $question4 = new Question();
            $question4->content = 'Nhà Toán Học Turing là người nước nào';
            $question4->answerA = 'Anh.';
            $question4->answerB = 'Pháp.';
            $question4->answerC = 'Italia.';
            $question4->answerD = 'Nga';
            $question4->correctAnswer = 'A';

            $question5 = new Question();
            $question5->content = 'Năm 1831 vị vua nào đã đổi tên Thăng Long thành Hà Nội?';
            $question5->answerA = 'Bảo Đại.';
            $question5->answerB = 'Minh Mạng.';
            $question5->answerC = 'Tự Đức.';
            $question5->answerD = 'Khải Định.';
            $question5->correctAnswer = 'B';

            $question6 = new Question();
            $question6->content = 'Sông Mê Kông chảy qua bao nhiêu quốc gia?';
            $question6->answerA = '5';
            $question6->answerB = '6';
            $question6->answerC = '7';
            $question6->answerD = '8';
            $question6->correctAnswer = 'B';

            $question7 = new Question();
            $question7->content = 'Trong bài hát "Đất nước" nhạc sỹ Phạm Minh Tuấn đã ví hình ảnh đất nước với hình ảnh nào ?';
            $question7->answerA = 'Lũy tre xanh.';
            $question7->answerB = 'Cánh sen hồng.';
            $question7->answerC = 'Bông lúa vàng.';
            $question7->answerD = 'Giọt đàn bầu.';
            $question7->correctAnswer = 'D';

            $question8 = new Question();
            $question8->content = '"Khi ta ở, chỉ là nơi đất ở - Khi ta đi, đất đã hóa tâm hồn" - Đây là hai câu thơ trong bài thơ nào của Chế Lan Viên?';
            $question8->answerA = 'Trên dải Trường Sơn.';
            $question8->answerB = 'Tiếng hát con tàu.';
            $question8->answerC = 'Tình ca ban mai.';
            $question8->answerD = 'Bay ngang mặt trời.';
            $question8->correctAnswer = 'C';

            $question9 = new Question();
            $question9->content = 'Đợt rét muộn cuối cùng vào cuối tháng Ba ở miền Bắc có tên là gì?';
            $question9->answerA = 'Rét Nàng Bân.';
            $question9->answerB = 'Rét Nàng Thơ.';
            $question9->answerC = 'Rét Nàng Thanh.';
            $question9->answerD = 'Rét Nàng May';
            $question9->correctAnswer = 'C';

            $question10 = new Question();
            $question10->content = 'Địa danh núi Bà Đen thuộc tỉnh nào?';
            $question10->answerA = 'Lào Cai.';
            $question10->answerB = 'Lâm Đồng.';
            $question10->answerC = 'Gia Lai';
            $question10->answerD = 'Tây Ninh';
            $question10->correctAnswer = 'D';


            $questions = array($question1, $question2, $question3, $question4, $question5, $question6, $question7, $question8, $question9, $question10);

        ?>

        <?php
            $cookie_name = "choices";
            $choices = json_decode($_COOKIE[$cookie_name], true);

            if ($_SERVER['REQUEST_METHOD'] == "POST") {
                $newChoices = array_merge($choices, $_POST);
                setcookie($cookie_name, json_encode($newChoices), time()+3600);

                header("Location: page3.php");
            }
        ?>
        <div class="wrapper">
            <h3 class="title">Kiểm tra nào!</h3>
            <form method="post">
                <?php
                    for ($i = 5; $i < 10; $i++) {
                        echo '<div class="question">
                                    <h4 class="question__content">' . ($i+1) . '. ' . $questions[$i]->content . '</h4>
                                    <input required type="radio" id="answerA' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="A">
                                    <label for="answerA' . ($i+1). '">A. ' . $questions[$i]->answerA . '</label><br>
                                    <input type="radio" id="answerB' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="B">
                                    <label for="answerB' . ($i+1). '">B. ' . $questions[$i]->answerB . '</label><br>
                                    <input type="radio" id="answerC' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="C">
                                    <label for="answerC' . ($i+1). '">C. ' . $questions[$i]->answerC . '</label><br>
                                    <input type="radio" id="answerD' . ($i+1) .'" name="userChoice' . ($i+1) . '" value="D">
                                    <label for="answerD' . ($i+1). '">D. ' . $questions[$i]->answerD . '</label><br>
                                </div>';
                    }
                ?>

                <br>
                <input type="submit" class="btn-submit" value="Nộp bài">
            </form>

        </div>

    </div>

</body>
</html>