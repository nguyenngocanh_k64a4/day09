<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Kiểm tra</title>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet">
    <style type="text/css">
        .main {
            display: flex;
            justify-content: center;
            align-items: center;
            min-height: 80vh;
            font-size: 16px;
        }

        .wrapper {
            width: 35%;
            padding: 20px 50px 50px;
            margin-top: 4px;
            margin-bottom: 60px;
            position: relative;
            border: 1px solid #ccc;
        }

        .title {
            text-align: center;
            color: rgba(50, 96, 168, 0.94);
            margin-bottom: 40px;
        }

        .score {
            color: rgb(52, 96, 168);
            margin-bottom: 20px;
        }

        .description {
            font-style: italic;
        }

    </style>
</head>
<body>
<div class="main">
    <?php
        class Question
        {
            public $content;
            public $answerA;
            public $answerB;
            public $answerC;
            public $answerD;
            public $correctAnswer;
        }

        $question1 = new Question();
        $question1->content = 'Vĩ tuyến 17 chạy qua địa phận tỉnh nào của nước ta?';
        $question1->answerA = 'Quảng Bình.';
        $question1->answerB = 'Quảng Trị.';
        $question1->answerC = 'Thanh Hóa.';
        $question1->answerD = 'Nghệ An.';
        $question1->correctAnswer = 'B';

        $question2 = new Question();
        $question2->content = 'Nằm bên dãy Hoàng Liên Sơn, Mù Căng Chải là huyện vùng sâu vùng xa của tỉnh nào?';
        $question2->answerA = 'Hà Giang.';
        $question2->answerB = 'Cao Bằng.';
        $question2->answerC = 'Yên Bái.';
        $question2->answerD = 'Lào Cai.';
        $question2->correctAnswer = 'C';

        $question3 = new Question();
        $question3->content = '"Bao giờ hết cỏ nước Nam thì hết người Nam đánh Tây" – Đây là câu nói của ai?';
        $question3->answerA = 'Ngô Thì Nhậm.';
        $question3->answerB = 'Trần Cao Vân.';
        $question3->answerC = 'Nguyễn Trung Trực.';
        $question3->answerD = 'Ngô Đức Kế.';
        $question3->correctAnswer = 'C';

        $question4 = new Question();
        $question4->content = 'Nhà Toán Học Turing là người nước nào';
        $question4->answerA = 'Anh.';
        $question4->answerB = 'Pháp.';
        $question4->answerC = 'Italia.';
        $question4->answerD = 'Nga';
        $question4->correctAnswer = 'A';

        $question5 = new Question();
        $question5->content = 'Năm 1831 vị vua nào đã đổi tên Thăng Long thành Hà Nội?';
        $question5->answerA = 'Bảo Đại.';
        $question5->answerB = 'Minh Mạng.';
        $question5->answerC = 'Tự Đức.';
        $question5->answerD = 'Khải Định.';
        $question5->correctAnswer = 'B';

        $question6 = new Question();
        $question6->content = 'Sông Mê Kông chảy qua bao nhiêu quốc gia?';
        $question6->answerA = '5';
        $question6->answerB = '6';
        $question6->answerC = '7';
        $question6->answerD = '8';
        $question6->correctAnswer = 'B';

        $question7 = new Question();
        $question7->content = 'Trong bài hát "Đất nước" nhạc sỹ Phạm Minh Tuấn đã ví hình ảnh đất nước với hình ảnh nào ?';
        $question7->answerA = 'Lũy tre xanh.';
        $question7->answerB = 'Cánh sen hồng.';
        $question7->answerC = 'Bông lúa vàng.';
        $question7->answerD = 'Giọt đàn bầu.';
        $question7->correctAnswer = 'D';

        $question8 = new Question();
        $question8->content = '"Khi ta ở, chỉ là nơi đất ở - Khi ta đi, đất đã hóa tâm hồn" - Đây là hai câu thơ trong bài thơ nào của Chế Lan Viên?';
        $question8->answerA = 'Trên dải Trường Sơn.';
        $question8->answerB = 'Tiếng hát con tàu.';
        $question8->answerC = 'Tình ca ban mai.';
        $question8->answerD = 'Bay ngang mặt trời.';
        $question8->correctAnswer = 'C';

        $question9 = new Question();
        $question9->content = 'Đợt rét muộn cuối cùng vào cuối tháng Ba ở miền Bắc có tên là gì?';
        $question9->answerA = 'Rét Nàng Bân.';
        $question9->answerB = 'Rét Nàng Thơ.';
        $question9->answerC = 'Rét Nàng Thanh.';
        $question9->answerD = 'Rét Nàng May';
        $question9->correctAnswer = 'C';

        $question10 = new Question();
        $question10->content = 'Địa danh núi Bà Đen thuộc tỉnh nào?';
        $question10->answerA = 'Lào Cai.';
        $question10->answerB = 'Lâm Đồng.';
        $question10->answerC = 'Gia Lai';
        $question10->answerD = 'Tây Ninh';
        $question10->correctAnswer = 'D';


        $questions = array($question1, $question2, $question3, $question4, $question5, $question6, $question7, $question8, $question9, $question10);

    ?>

    <?php
        $cookie_name = "choices";
        $choices = json_decode($_COOKIE[$cookie_name], true);

        $score = 0;
        for ($i = 0; $i < 10; $i++) {
            $key = "userChoice" . strval($i+1);

            if ($choices[$key] === $questions[$i]->correctAnswer) {
                $score++;
            }
        }
    ?>
    <div class="wrapper">
        <h3 class="title">Kết quả</h3>
        <h4 class="score">Điểm của bạn: <?php echo $score; ?>/10.</h4>
        <h4 class="description">
            =>
            <?php
                if ($score < 4) {
                    echo 'Bạn quá kém, cần ôn tập thêm!';
                } elseif ($score <= 7) {
                    echo 'Cũng bình thường!';
                } else {
                    echo 'Sắp sửa làm được trợ giảng lớp PHP!';
                }
            ?>
        </h4>

    </div>

</div>

</body>
</html>